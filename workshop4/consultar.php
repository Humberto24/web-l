<?php
require 'logica/consultas.php';
?>

<!DOCTYPE html>
<html lang="en">    
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Consultar datos</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<body>
    <nav style="background-color:#00796b;">
        <center>
            <h1 style="color:white;">Listado de matriculas</h1>
        </center>
    </nav>

    <center>
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Nombre</th>
                    <th scope="col">Apellidos</th>
                    <th scope="col">Email</th>
                    <th scope="col">Carrera</th>
                </tr>
            </thead>
        <tbody id="datos">
            <?php
            foreach ($query as $row){?>
            <tr>
                <td><?php echo $row['nombre'];?></td> 
                <td><?php echo $row['apellidos'];?></td>
                <td><?php echo $row['email'];?></td>
                <td><?php echo $row['nombreCarreras'];?></td>
            </tr>
        </tbody>

        <?php
        }
        ?>
        </table>
    </center>

</body>
</html>