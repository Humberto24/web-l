<?php
  require 'logica/select.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Insertar datos</title>
  <link rel="stylesheet" href="css/estilo.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
    crossorigin="anonymous"></script>
</head>

<body>


  <div id="wrapper" class="container">

    <center>

      <h1>Registrar matriculas</h1>
      <hr>
      <form method="POST" name="form-work" action="logica/guardar.php">

        <fieldset>
        
        <div class="form-group">
            <div class="col-md-6">
              <label class="control-label" for="nome">Cedula</label>
              <input name="cedula" class="form-control" placeholder="Cedula" type="text">
            </div>
          </div>


          <div class="form-group">
            <div class="col-md-6">
              <label class="control-label" for="nome">Nombre</label>
              <input name="nombre" class="form-control" placeholder="Tu nombre" type="text">
            </div>
          </div>

          <div class="form-group">
            <div class="col-md-6">
              <label class="control-label" for="surname">Apellidos</label>
              <input name="apellido" class="form-control" placeholder="Apellidos" type="text">
            </div>
          </div>

          <div class="form-group">
            <div class="col-md-6">
              <label class="control-label" for="nome">Email</label>
              <input name="email" class="form-control" placeholder="Email" type="text">
            </div>
          </div>

          <div class="form-group"> 
            <div class="col-md-6">                                      
              <label for="idcarrera">Carrera</label><br/>
              <select name="idCarrera" class="form-control" data-live-search="true">
              <?php
              foreach ($query as $row){?>
              <option value="<?php echo $row['id']; ?>"><?php echo $row['nombreCarreras']; ?></option>
                  <?php
                      }
                      ?>
              </select> 
            </div>                                          
           </div>

          <div class="form-group">
            <div class="col-md-12">
              <button type="submit" class="btn btn-primary btn-lg btn-block info">Guardar</button>
            </div>
          </div>
          <button type="button" class="btn btn-primary btn-lg btn-block info"><a style="color: white;" href="consultar.php">Consultar matriculas</a></button>
        </fieldset>
      </form>
    </center>
  </div>
</body>
</html>