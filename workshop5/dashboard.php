  
<?php
  session_start();

  $user = $_SESSION['user'];
  if (!$user) {
    header('Location: index.php');
  }
  ?>

  <h1> Bienvenido <?php echo $user['nombre'];?> <?php echo $user['apellido'] ?> </h1>
  <a href="logout.php">Logout</a>

  <nav class="nav">
    <?php  if($user['tipo'] === 'Administrador') { ?>
        <?php
            header('Location: index1.php');
        ?>
    <?php } ?>
  </nav>

