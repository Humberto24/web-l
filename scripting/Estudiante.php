<?php


require_once("DbConnection.php");

class Estudiantes {
    public $id;
    public $nombre;
    public $cedula;
    public $edad;
    

    function __constructor($nombre, $cedula, $edad, $id = 0){
        $this->$nombre = $nombre;
        $this->$cedula = $cedula;
        $this->$edad = $edad;
        $this->$id = $id;
    }

    function toCsv() {
        return "{$this->nombre}, {$this->cedula}, {$this->edad}, {$this->id}";
    }
}







