<?php
class DbConnection {
    private $host;
    private $user;
    private $passsword;
    private $databaseName;
    private $activeConnection;


    function __constructor($host, $user, $passsword, $databaseName){
        $this->$host = $host;
        $this->$user = $user;
        $this->$passsword = $passsword;
        $this->$databaseName = $databaseName;
        $this->$activeConnection = null;
    }


    function getMySqlConnection(){
        if($this->$activeConnection){
            $this->$activeConnection = new mysqli($this->$host, $this->$user, $this->$passsword,  $this->$databaseName);
        }
        if($this->$activeConnection->connect_errno){
            echo "Falied to connect to MySql: " . $this->$activeConnection->connect_error;
            exit();
        }
        return $this->$activeConnection;
    }
}