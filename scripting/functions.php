<?php

require_once("EstudianteDAO.php");
require_once("Estudiante.php");

$students = EstudianteDAO.getAll();

foreach($students as $student){
    $newStudent = new Estudiante($student["nombre"], $student["cedula"], $student["edad"], $student["id"]);
    echo $newStudent->toCsv().PHP_EOL;
}